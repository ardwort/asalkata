<?php
/**
 * @updated 2013-12-07 09:31
 */
class asalkata
{
    public $is_home;
    public $body;
    public $title;

    private $db;
    private $languages;
    private $languages2;

    private $module;
    private $action;
    private $query;
    private $language;

    private $rewriteUrl = true;

    /*
     *
     */
    function __construct($dsn)
    {
        $this->db = new db;
        $this->db->connect($dsn);
        $this->is_home = true;
        $this->title = 'asalkata ~ Kamus etimologis bahasa Indonesia';
        $sql = 'SELECT abbrev, label FROM sys_abbrev WHERE is_active = 1 AND type IN (\'lang\', \'region\') ORDER BY abbrev;';
        if ($tmp = $this->db->get_row_assoc($sql, 'abbrev', 'label', false)) {
            foreach ($tmp as $key => $val) {
                $this->languages[strtoupper($key)] = $val;
            }
            $this->languages2 = $tmp;
        }
    }

    /*
     *
     */
    function get_content()
    {
        global $_GET;

        $this->interpretCommand();

        $_GET['op'] = 1;
        if ($this->word) {
            $this->query = $this->word;
            $_GET['op'] = 3;
        }

        $op_template = 'a.%1$s %2$s \'%3$s%4$s%5$s\'';
        $operators = array(
            '1' => array('type'=>'LIKE', 'open'=>'%', 'close'=>'%'),
            '2' => array('type'=>'REGEXP', 'open'=>'[[:<:]]', 'close'=>'[[:>:]]'),
            '3' => array('type'=>'=', 'open'=>'', 'close'=>''),
            '4' => array('type'=>'LIKE', 'open'=>'', 'close'=>'%'),
            '5' => array('type'=>'LIKE', 'open'=>'%', 'close'=>''),
        );
        if (!array_key_exists($_GET['op'], $operators)) $_GET['op'] = '1';
        $op_open = $operators[$_GET['op']]['open'];
        $op_close = $operators[$_GET['op']]['close'];
        $op_type = $operators[$_GET['op']]['type'];

        $where = ' AND a.is_hidden = 0 AND a.in_kbbi = 1';
        if ($this->query) {
            $this->is_home = false;
            $where .= ' AND ' . sprintf($op_template, 'phrase_normal', $op_type, $op_open,
                $this->db->quote($this->query, null, false), $op_close);
        }

        if (isset($this->query) || isset($this->language) || isset($this->tag)) {
            $this->is_home = false;
        }

        if (isset($this->query)) {
            $this->query = trim($this->query);
            if ($this->query != '') {
                $this->log_search($this->query);
                if ($this->word == '') {
                    $this->title = sprintf('Asal kata mirip <em><a href="%s">%s</a></em>',
                        $this->getUrl('word', $this->query), $this->query);
                } else {
                    $this->title = sprintf('Asal kata <em>%s</em>', $this->query);
                }
            } else {
                $this->title = sprintf('Daftar serapan');
            }
        } else {

        }

        $found = true;

        // Language
        if ($this->language) {
            $key = strtoupper($this->language);
            if (array_key_exists($key, $this->languages)) {
                $this->title = sprintf('Daftar serapan dari %s', lcfirst($this->languages[$key]));
            } else {
                $found = false;
            }
            $where .= ' AND LOWER(a.src_lang) = ' . $this->db->quote(strtolower($this->language));
        }

        // Final
        $cols = 'a.phrase, a.phrase_normal, a.homonym, a.def_id, a.def_en,
            a.src_lang, a.src_blend, a.src_word, a.src_meaning, a.src_root,
            a.notes, a.reference, a.raw_data,
            b.homonyms, b.hom_lwim, b.hom_kbbi';
        $from = 'FROM etymology a, words b ';

        // Tag
        if ($this->tag) {
            $this->title = sprintf('Daftar serapan berlabel "%s"', $this->tag);
            $from .= ', tags c ';
            $from .= 'WHERE a.phrase collate utf8_bin = b.word AND a.phrase collate utf8_bin = c.phrase AND a.homonym = c.homonym AND c.tag = ' . $this->db->quote($this->tag). ' ';
            $from .= $where;
        } else {
            $from .= 'WHERE a.phrase collate utf8_bin = b.word ';
            $from .= $where;
        }

        $from .= ' ORDER BY a.sort_idx ';
        $this->db->defaults['rperpage'] = 100;

        if (!$this->is_home) {
            if ($rows = $this->db->get_rows_paged($cols, $from)) {
                $this->get_tags($rows);
                // $body .= $this->db->get_page_nav(true);
                $body .= '<dl class="dl-horizontal">' . LF;
                foreach ($rows as $row) {
                    $body .= sprintf('<dt>%s<dt>', $this->format_phrase($row)). LF;
                    $body .= sprintf('<dd>%s</dd>', $this->format_source($row)). LF;
                }
                $body .= '</dl>' . LF;
                $body .= $this->db->get_page_nav(true);
            } else {
                $found = false;
            }
            if (!$found) {
                if ($random_data = $this->get_random_phrase()) {
                    $random .= ' Sudah tahu asal kata '. $random_data['phrase_linked'] . '?';
                }
                $body .= '<div class="alert alert-warning alert-dismissable">' . LF;
                $body .= 'Duh, data yang Anda minta tidak ditemukan. Silakan masukkan pencarian lain.' . $random;
                $body .= '</div>' . LF;
            }
            $body = sprintf('<h2>%s</h2>', $this->title) . LF . $body;
            $this->title .= ' ~ asalkata';
        } else {
            $body .= '<div class="text-center">'. LF;
            if ($random_data = $this->get_random_phrase()) {
                $random = sprintf('%s berasal dari %s %s',
                    $random_data['phrase_linked'],
                    $random_data['src_lang_linked'],
                    $random_data['src_word_formatted']);
                $random = 'Tahukah Anda bahwa kata '. trim($random) . '?';
                $body .= '<p>' . $random . '</p>'. LF;
            }
            $body .= '</div>'. LF;
        }

        return($body);
    }

    /*
     *
     */
    function get_language_list()
    {
        if (!is_array($this->languages2)) return;
        $ret = '';
        $ret .= '<ul class="list-inline">' . LF;
        foreach ($this->languages2 as $key => $val) {
            $url = $this->getUrl('language', $key);
            $ret .= sprintf('<li><a href="%s" title="%s">%s</a></li>',
                $this->getUrl('language', $key), $val, $key);
        }
        $ret .= '</ul>' . LF;
        return($ret);
    }

    /*
     *
     */
    function get_random_phrase()
    {
        $sql = 'SELECT a.phrase, a.src_lang, a.src_word FROM etymology a
            WHERE a.is_hidden = 0 AND a.in_kbbi = 1 and a.homonym = 1 and a.src_blend = 0
            ORDER BY RAND() LIMIT 1;';
        if ($row = $this->db->get_row($sql)) {
            $ret['phrase'] = $row['phrase'];
            $ret['src_lang'] = $row['src_lang'];
            $ret['src_word'] = $row['src_word'];
            $ret['phrase_linked'] = sprintf('<em><a href="%s">%s</a></em>',
                $this->getUrl('word', $row['phrase']), $row['phrase']);
            $ret['src_lang_linked'] = $row['src_lang'];
            $ret['src_word_formatted'] = $ret['src_word'];
            if (is_array($this->languages)) {
                $lang_name = lcfirst($this->languages[strtoupper($row['src_lang'])]);
                $ret['src_lang_linked'] = sprintf('<a href="%s">%s</a>',
                    $this->getUrl('language', $row['src_lang']), $lang_name);
            }
            if ($ret['src_word']) {
                $ret['src_word_formatted'] = sprintf('<em>%s</em>', $row['src_word']);
            }
        }
        return($ret);
    }

    /*
     *
     */
    function get_tags(&$rows)
    {
        $row_count = count($rows);
        for ($i = 0; $i < $row_count; $i++) {
            $row = $rows[$i];
            $words .= $words ? ', ' : '';
            $words .= $this->db->quote($row['phrase']);
            $pairs[$row['phrase'] . $row['homonym']] = $i;
        }
        $sql = 'SELECT phrase, homonym, tag FROM tags WHERE phrase IN (%s)';
        $sql = sprintf($sql, $words);
        if ($tags = $this->db->get_rows($sql)) {
            foreach ($tags as $tag) {
                $key = $tag['phrase'] . $tag['homonym'];
                $rows[$pairs[$key]]['tags'][] = $tag['tag'];
            }
        }
    }

    /*
     * Log searched phrase
     */
    function getUrl($module, $param)
    {
        switch ($module) {
            case 'language':
                $qs[0] = $module;
                $qs[1] = $param;
                break;
            case 'tag':
                $qs[0] = $module;
                $qs[1] = $param;
            case 'word':
                $qs[0] = $module;
                $qs[1] = $param;
                break;
            default:
                $qs[0] = 'search';
                $qs[1] = $param;
                break;
        }
        $url = ROOT;
        if (is_array($qs)) {
            foreach ($qs as $val) {
                $url .= '/' . $val;
            }
            $url = trim($url);
            if (substr($url, -1, 1) == '/') {
//                $url = substr($url, strlen($url) - 1, 1);
            }
        }
        return($url);
    }

    /*
     *
     */
    function format_phrase(&$row)
    {
        $ret = $row['phrase'];
        if ($row['homonyms'] > 1) {
            // $ret .= sprintf(' (%s)', roman_numerals($row['homonym']));
            $ret .= sprintf(' (%s)', $row['homonym']);
        }
        $url = sprintf('http://kateglo.com/?mod=dict&action=view&phrase=%s', $row['phrase_normal']);
        $url = $this->getUrl('word', $row['phrase_normal']);
        $ret = sprintf('<phrase><a href="%s">%s</a></phrase>', $url, $ret);
        return($ret);
    }

    /*
     *
     */
    function format_source(&$row)
    {
        global $_GET;

        $tmp = $row['src_word'];

        // Source language & word
        $ret .= ' <span class="glyphicon glyphicon-chevron-left"></span>';
        if (!$row['src_blend']) {
            if ($row['src_lang']) {
                $ret .= $this->format_language($row['src_lang']);
            }
            $ret .= $this->format_source_word($tmp, $row['src_lang']);
            if ($row['src_meaning']) {
                $ret .= sprintf(' <sourcemeaning>%s</sourcemeaning>', $row['src_meaning']);
            }
        } else {
            $parts = explode('+', $tmp);
            foreach ($parts as $part) {
                $part = trim($part);
                if ($part) {
                    $word .= $word ? ' + ' : '';
                    unset($lang);
                    if (preg_match('/^(.+) \(([A-Z])([^\)]+)\)$/', $part, $word_lang)) {
                        $lang = $word_lang[2] . $word_lang[3];
                        $word .= $this->format_language($lang);
                        $word .= $this->format_source_word($word_lang[1], $lang);
                    } else {
                        $word .= $this->format_source_word($part, $lang);
                    }
                }
            }
            $ret .= $word;
        }

        // Other attributes

        if ($row['src_root']) $ret .= $this->format_source_root($row['src_root']);
        if ($row['tags']) $ret .= $this->format_tags($row['tags']);
        if ($row['reference']) $ret .= $this->format_reference($row['reference'], $row['phrase']);
        $ret .= $this->format_definition($row);
        if ($row['notes']) $ret .= sprintf('<br /><notes>%s<notes>', $row['notes']);

        return($ret);
    }

    /*
     *
     */
    function format_source_word($word, $language)
    {
        if (trim($word) == '') '';
        $url = 'http://www.etymonline.com/index.php?term=' . $word;
        if ($language != 'Ing') {
            $ret .= $word;
        } else {
            $ret .= sprintf('<a href="%s" target="etymonline">%s</a>', $url, $word);
        }
        $ret = sprintf(' <sourceword target="etymonline">%s</sourceword>', $ret);
        return($ret);
    }

    /*
     *
     */
    function format_language($lang)
    {
        if (is_array($this->languages) && $lang != '') {
            $key = strtoupper($lang);
            if (array_key_exists($key, $this->languages)) {
                $title = $this->languages[$key];
                $attrib = sprintf(' title="%s"', $title);
            }
        }
        $lang = sprintf('<a href="%s"%s>%s</a>',
            $this->getUrl('language', $lang), $attrib, $lang);
        $ret = sprintf(' <sourcelang>%s</sourcelang>', $lang);
        return($ret);
    }

    /*
     *
     */
    function format_source_root($input)
    {
        $ret = '';
        $roots = explode('<', $input);
        foreach ($roots as $root) {
            $root = trim($root);
            if ($root != '') {
                $ret .= $ret ? ' &lt; ' : '';
                if (preg_match('/^(.+) \(([A-Z])([^\)]+)\)$/', $root, $word_lang)) {
                    $lang = $word_lang[2] . $word_lang[3];
                    $ret .= $this->format_source_word($word_lang[1], $lang);
                    $ret .= $this->format_language($lang);
                } else {
                    $ret .= $this->format_source_word($root, $lang);
                }
            }
        }
        if ($ret) {
            $ret = sprintf(' &lt; <sourceroot>%s<sourceroot>', $ret);
        }
        return($ret);
    }

    /*
     *
     */
    function format_definition(&$row) {
        if (!$row['def_id'] && !$row['def_en']) return;
        if ($row['def_id']) {
            $ret .= sprintf('<defid>%s</defid>', $row['def_id']);
        }
        if ($row['def_en']) {
            $ret .= $ret ? '; ' : '';
            $ret .= sprintf('<defen><em>%s</em></defen>', $row['def_en']);
        }
        $ret = sprintf(' <definitions>%s</definitions>', $ret);
        return($ret);
    }

    /*
     *
     */
    function format_reference($ref, $phrase)
    {
        $content = $ref;
        $refs = array(
            'LWIM' => array(
                'label' => 'Russell Jones (ed.) (2007) Loan-Words in Indonesian and Malay',
                'url' => 'http://sealang.net/lwim/search.pl?dict=lwim&ignoreDiacritic=1&orth=%s',
                ),
            'KBBI3' => array(
                'label' => 'Pusat Bahasa (2005) Kamus Besar Bahasa Indonesia Edisi III',
                'url' => 'http://kateglo.com/?&mod=dictionary&action=view&phrase=%s',
                ),
            );
        if (array_key_exists($ref, $refs)) {
            $url = sprintf($refs[$ref]['url'], $phrase);
            $label = '<span class="glyphicon glyphicon-bookmark ref-glyph"></span>' . $ref;
            $label = $ref;
            $content = sprintf('<a href="%s" target="reference">%s</a>', $url, $label);
            $title = sprintf(' title="%s"', $refs[$ref]['label']);
        }
        $ret .= sprintf(' <reference%s>%s</reference>', $title, $content);
        return($ret);
    }

    /*
     *
     */
    function format_tags($tags)
    {
        //$ret .= ' <span class="glyphicon glyphicon-tag" style="font-size: smaller; padding-left: 5px;"></span>';
        foreach ($tags as $tag) {
            $ret .= sprintf('<tag><a href="%s">%s</a></tag>',
                $this->getUrl('tag', $tag), $tag);
        }
        if ($ret) $ret = ' ' . $ret;
        return($ret);
    }

    /*
     * Log searched phrase
     */
    function log_search($phrase)
    {
        $sql = 'SELECT ety_id FROM etymology WHERE phrase_normal = %s;';
        $sql = sprintf($sql, $this->db->quote($phrase));
        $found = $this->db->get_rows($sql) ? 1 : 0;
        $sql = 'UPDATE searched SET search_count = search_count + 1, found = %s WHERE phrase = %s';
        $sql = sprintf($sql, $found, $this->db->quote($phrase));
        $rows = $this->db->exec($sql);
        if (!$rows) {
            $sql = 'INSERT INTO searched (phrase, first, found) VALUES (%s, \'%s\', %s)';
            $sql = sprintf($sql, $this->db->quote($phrase), date('c'), $found);
            $rows = $this->db->exec($sql);
        }
    }

    function interpretCommand()
    {
        global $_GET;

        if ($_GET['load']) {
            $parameters = explode('/', $_GET['load']);
            $this->module = $parameters[0];
            switch ($this->module) {
                case 'language':
                    $this->language = $parameters[1];
                    break;
                case 'tag':
                    $this->tag = $parameters[1];
                    break;
                case 'word':
                    $this->word = $parameters[1];
                    break;
                default:
                    $this->module = 'search';
                    $this->query = $parameters[1];
                    break;
            }
        }

        // override
        if (isset($_GET['q'])) {
            $url = $this->getUrl('search', $_GET['q']);
            header('Location: ' . $url);
            $this->module = 'search';
            $this->query = $_GET['q'];
        } elseif ($_GET['lang']) {
            $this->module = 'language';
            $this->language = $_GET['lang'];
        } elseif ($_GET['tag']) {
            $this->module = 'tag';
            $this->tag = $_GET['tag'];
        } elseif ($_GET['t']) {
            $this->module = 'word';
            $this->word = $_GET['t'];
        }
    }

}