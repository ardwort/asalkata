<?php
/**
 * Sample config file. Change name into "config.php"
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);

$dsn['phptype'] = 'mysqli';
$dsn['hostspec'] = 'localhost';
$dsn['database'] = 'my_asalkata';
$dsn['username'] = '';
$dsn['password'] = '';
$dsn['charset'] = 'utf8';

$ROOT_DIR = __DIR__;

define(LF, "\r\n");
