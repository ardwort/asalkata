<?php
/**
 * Main landing script
 *
 * @updated 2013-12-07 09:28
 */
include_once('config.php');
ini_set('include_path', $ROOT_DIR . '/pear/');
require_once('lib/functions.php');
require_once('lib/sealang.php');
require_once('lib/db.php');
require_once('lib/asalkata.php');

$asalkata = new asalkata($dsn);
$body = $asalkata->get_content();
?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo(strip_tags($asalkata->title)); ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="keywords" content="asal kata, asal usul kata, etimologi, kata, bahasa Indonesia, etymology, Indonesian language" />
<meta name="description" content="asalkata adalah kamus etimologis bahasa Indonesia" />
<link rel="stylesheet" href="<?php echo(ROOT); ?>/assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo(ROOT); ?>/assets/css/asalkata.css" />
<link rel="shortcut icon" href="<?php echo(ROOT); ?>/assets/img/favicon.png" />
</head>
<body class="<?php echo($asalkata->is_home ? 'home' : 'page') ?>">

<div class="container">

<?php if ($asalkata->is_home) { ?>
    <div class="text-center">
        <p><img src="<?php echo(ROOT); ?>/assets/img/asalkata-250px.png" width="250" height="57" alt="asalkata" title="asalkata"></p>
        <p class="lead">Selamat datang di asalkata, kamus etimologis bahasa Indonesia</p>
        <p>Silakan masukkan kata bahasa Indonesia yang ingin dicari asalnya pada kotak pencarian, atau langsung tekan tombol kaca pembesar untuk menelusuri daftar kata yang tersedia.</p>
    </div>
    <div id="div_search">
        <form id="frm_search" method="get" action="<?php echo(ROOT); ?>/search" class="form-inline" role="form">
            <div class="row"><div class="col-sm-6 col-sm-offset-3">
            <div class="input-group">
                <input id="q" name="q" type="text" class="form-control input-md" placeholder="Cari asal kata ..." value="<?php echo($_GET['q']); ?>">
                <span class="input-group-btn">
                    <button id="btn_search" class="btn btn-primary btn-md" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            </div></div>
        </form>
    </div>
<?php } else { ?>
    <div id="header">
        <form id="frm_search" method="get" action="<?php echo(ROOT); ?>/search" class="form-inline" role="form">
            <div class="row">
            <div class="col-xs-6">
                <a href="<?php echo(ROOT); ?>/"><img src="<?php echo(ROOT); ?>/assets/img/asalkata-25px.png" width="140" height="25" alt="asalkata" title="asalkata" /></a>
            </div>
            <div class="col-xs-6">
                <div class="input-group">
                <input id="q" name="q" type="text" class="form-control input-sm" placeholder="Cari asal kata ..." value="<?php echo($_GET['q']); ?>">
                <span class="input-group-btn">
                    <button id="btn_search" class="btn btn-primary btn-sm" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
                </div>
            </div>
            </div>
        </form>
    </div>
<?php } ?>

<!-- BEGIN MAIN CONTENT -->

<?php echo($body); ?>

<!-- END MAIN CONTENT -->

    <div id="languages">
<?php echo($asalkata->get_language_list()); ?>
    </div>

    <div id="footer">
        <ul class="list-inline">
            <li><a href="#" data-toggle="modal" data-target="#ihwal">Ihwal</a></li>
            <li><a href="#" data-toggle="modal" data-target="#api">API</a></li>
            <li><a href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.id">CC BY-NC-SA</a></li>
        </ul>
    </div>

    <div class="modal fade" id="ihwal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Ihwal</h4>
                </div>
                <div class="modal-body"><p>asalkata berstatus <a href="https://en.wikipedia.org/wiki/Software_release_life_cycle#Alpha">alfa</a>. Fitur, konten, dan tampilan masih sangat mungkin berubah setiap saat. Meski <a href="https://bitbucket.org/ardwort/asalkata">kode programnya</a> masih amburadul, asalkata <strike>terpaksa</strike> dipaksa diterbitkan agar menimbulkan semangat untuk menyelesaikannya. Sila baca <a href="README.md">README</a> untuk informasi yang (sedikit) lebih lengkap.</p></div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="api">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">API</h4>
                </div>
                <div class="modal-body"><p>Suatu hari nanti, API pasti disediakan karena apalah artinya aplikasi web bila tanpa API? Saat ini, kerja difokuskan dulu kepada pengembangan konten dan fitur. Sabar, ya.</p></div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo(ROOT); ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo(ROOT); ?>/assets/js/bootstrap.min.js"></script>
<script>
$(function() {
    $("#btn_search").click( function() {
         $("#frm_search").submit();
    });
});
</script>
</body>
</html>