DROP TABLE IF EXISTS etymology;
CREATE TABLE IF NOT EXISTS etymology (
  ety_id int(11) NOT NULL AUTO_INCREMENT,
  phrase varchar(255) NOT NULL,
  phrase_normal varchar(255) NOT NULL,
  sort_idx varchar(255) NOT NULL,
  homonym tinyint(4) NOT NULL DEFAULT '1',
  is_hidden tinyint(4) NOT NULL DEFAULT '0',
  in_kbbi tinyint(4) NOT NULL DEFAULT '0',
  src_blend tinyint(4) NOT NULL DEFAULT '0',
  src_lang varchar(30) DEFAULT NULL,
  src_word varchar(1000) DEFAULT NULL,
  src_meaning varchar(255) DEFAULT NULL,
  src_root varchar(1000) DEFAULT NULL,
  lwim int(11) DEFAULT NULL,
  reference varchar(30) DEFAULT NULL,
  data_flag varchar(30) DEFAULT NULL,
  notes varchar(4000) DEFAULT NULL,
  raw_data text,
  updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (ety_id),
  KEY phrase (phrase),
  KEY lwim (lwim),
  KEY sort_idx (sort_idx),
  KEY phrase_normal (phrase_normal),
  KEY src_lang (src_lang)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS searched;
CREATE TABLE IF NOT EXISTS searched (
  srch_id int(11) NOT NULL AUTO_INCREMENT,
  phrase varchar(255) NOT NULL,
  `first` datetime NOT NULL,
  `last` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  search_count int(11) NOT NULL DEFAULT '1',
  `found` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (srch_id),
  KEY phrase (phrase)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS sys_abbrev;
CREATE TABLE IF NOT EXISTS sys_abbrev (
  abbrev varchar(16) NOT NULL,
  label varchar(255) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  is_active tinyint(4) NOT NULL DEFAULT '0',
  updated datetime DEFAULT NULL,
  updater varchar(32) DEFAULT NULL,
  PRIMARY KEY (abbrev)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS words;
CREATE TABLE IF NOT EXISTS words (
  word varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  homonyms tinyint(4) NOT NULL DEFAULT '0',
  hom_lwim tinyint(4) NOT NULL DEFAULT '0',
  hom_kbbi tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (word)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `homonym` tinyint(4) NOT NULL DEFAULT '1',
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `phrase` (`phrase`,`homonym`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
