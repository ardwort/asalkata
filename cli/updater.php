<?php
/**
 *
 */
$rel_path = '../';
include_once($rel_path . 'config.php');
ini_set('include_path', $ROOT_DIR . '/pear/');
require_once($rel_path . '/lib/sealang.php');
require_once($rel_path . '/lib/db.php');

$updater = new updater($dsn);
$updater->get_see();

class updater
{
    function __construct($dsn)
    {
        $this->sea = new sealang();
        $this->db = new db;
        $this->db->connect($dsn);
    }

    // Set in_kbbi to 1
    function set_in_kbbi()
    {
        $sql = 'SELECT a.ety_id, a.phrase FROM etymology2 a, phrase b WHERE a.phrase = b.phrase';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $sql = 'UPDATE etymology2 SET in_kbbi = 1 WHERE ety_id = ' . $row['ety_id'];
                $this->db->exec($sql);
                echo(sprintf('%s (%s/%s)', $row['phrase'],
                    $i + 1, $rec_num) . LF);
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    // Set homonyms
    function set_lwim_homonyms()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.raw_data FROM etymology2 a';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $dom = new DOMDocument;
                $dom->loadHTML($row['raw_data']);
                $xpath = new DOMXPath($dom);
                $node = $xpath->query('//entry')->item(0);
                $sql = 'UPDATE etymology2 SET homonym = %s, lwim = %s WHERE ety_id = %s';
                $sql = sprintf($sql,
                    $node->getAttribute('hom'),
                    str_replace('LWIM:', '', $node->getAttribute('id')),
                    $row['ety_id']);
                $this->db->exec($sql);
                echo(sprintf('%s (%s/%s)', $row['phrase'],
                    $i + 1, $rec_num) . LF);
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    // Set LWIM ID
    function set_lwim2()
    {
        $sql = 'SELECT a.lw_id, a.lw_content FROM loan_words a';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $dom = new DOMDocument;
                $dom->loadHTML($row['lw_content']);
                $xpath = new DOMXPath($dom);
                $node = $xpath->query('//entry')->item(0);
                $sql = 'UPDATE loan_words SET lwim = %s WHERE lw_id = %s';
                $sql = sprintf($sql,
                    str_replace('LWIM:', '', $node->getAttribute('id')),
                    $this->db->quote($row['lw_id']));
                $this->db->exec($sql);
                echo(sprintf('%s (%s/%s)', $row['lw_id'],
                    $i + 1, $rec_num) . LF);
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    // Set homonyms
    function set_blend()
    {
        $trans = array(
            'Sanskrit' => 'Skt',
            'Javanese' => 'Jw',
            'Dutch' => 'Bld',
            'English' => 'Ing',
            'Latin' => 'Lt',
            'Arabic' => 'Ar',
            'Greek' => 'Yn',
            'French' => 'Pr',
            'Fr' => 'Pr',
            'Portuguese' => 'Prt',
            );
        $sql = 'SELECT a.ety_id, a.phrase, a.src_word, a.raw_data FROM etymology2 a WHERE a.src_word LIKE \'% (%\'';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $word = $row['src_word'];
                foreach ($trans as $key => $val) {
                    $word = str_replace('(' . $key, '(' . $val, $word);
                }
                $sql = 'UPDATE etymology2 SET src_blend = 1, src_word = %s WHERE ety_id = %s';
                $sql = sprintf($sql, $this->db->quote($word), $row['ety_id']);
                $this->db->exec($sql);
                echo(sprintf('%s (%s/%s)', $row['phrase'],
                    $i + 1, $rec_num) . LF);
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    // Set homonyms
    function remove_ref()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.src_word FROM etymology a WHERE a.src_word LIKE \'%Monier-Williams%\'';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $word = $row['src_word'];
                $word = preg_replace('Monier-Williams[0-9\.]+', '', $word);
                $sql = 'UPDATE etymology SET src_word = %s WHERE ety_id = %s';
                $sql = sprintf($sql, $this->db->quote($word), $row['ety_id']);
                $this->db->exec($sql);
                echo(sprintf('%s (%s/%s)', $row['phrase'],
                    $i + 1, $rec_num) . LF);
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function curl_diacritic_data()
    {
        $sql = 'SELECT a.lw_id, a.lw_content FROM loan_words a WHERE is_existed = 0;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $phrase = $row['lw_id'];
                unset($this->sea->raw_html);
                unset($this->sea->entries);
                $this->sea->curl_etymology($phrase);
                $this->sea->parse_etymology();
                $sql = 'INSERT INTO etymology2 (phrase, lwim, src_lang, src_word, raw_data) VALUES (%s, %s, %s, %s, %s);';
                foreach ($this->sea->entries as $entry) {
                    $sql = sprintf($sql,
                        $this->db->quote($entry['entry']),
                        $this->db->quote($entry['lwim']),
                        $this->db->quote($entry['lang']),
                        $this->db->quote($entry['word']),
                        $this->db->quote($entry['raw_xml']));
                    $this->db->exec($sql);
                    echo(sprintf('%s (%s/%s)', $entry['entry'],
                        $i + 1, $rec_num) . LF);
                }
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function get_diacritic_lwim()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.phrase_normal, a.raw_data FROM etymology a WHERE reference = \'LWIM\';';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                unset($this->sea->entries);
                $this->sea->raw_html = $row['raw_data'];
                $this->sea->parse_etymology();
                $sql = 'UPDATE etymology SET phrase = %s WHERE ety_id = %s;';
                foreach ($this->sea->entries as $entry) {
                    $sql = sprintf($sql,
                        $this->db->quote($entry['diacritic']),
                        $row['ety_id']);
                    $this->db->exec($sql);
                    echo(sprintf('%s (%s/%s)', $entry['entry'],
                        $i + 1, $rec_num) . LF);
                }
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function get_diacritic_kbbi()
    {
        $sql = 'SELECT a.phrase, a.pronounciation FROM phrase a ' .
            'WHERE a.pronounciation IS NOT NULL ORDER BY a.phrase;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $sql = 'UPDATE etymology a SET a.phrase = %s ' .
                    'WHERE a.reference = \'KBBI3\' AND a.phrase_normal = %s';
                $sql = sprintf($sql,
                    $this->db->quote($row['pronounciation']),
                    $this->db->quote($row['phrase']));
                $this->db->exec($sql);
                echo(sprintf('%s (%s/%s)', $row['phrase'],
                    $i + 1, $rec_num) . LF);
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function get_kbbi()
    {
        $sql = 'SELECT abbrev, label FROM sys_abbrev WHERE type IN (\'lang\', \'region\');';
        if ($langs = $this->db->get_row_assoc($sql, 'abbrev', 'label')) {
            foreach ($langs as $key => $val) {
                $langs[$key] = array('label' => $val, 'count' => 0);
            }
        }
        $insert = 'INSERT INTO etymology2 (reference, in_kbbi, phrase, src_lang) VALUES (\'KBBI3\', 1, %s, %s)';
        $sql = 'SELECT a.phrase, a.info FROM phrase a WHERE a.phrase_type = \'r\' AND a.info IS NOT NULL;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $infos = explode(',', $row['info']);
                foreach ($infos as $info) {
                    if (trim($info)) {
                        if (array_key_exists($info, $langs)) {
                            $sql = sprintf($insert,
                                $this->db->quote($row['phrase']),
                                $this->db->quote($info));
                            $this->db->exec($sql);
                            $langs[$info]['count']++;
                        }
                    }
                }
            }
        }
        $sql = 'UPDATE etymology2 SET sort_idx = phrase WHERE LEFT(phrase, 1) != \'-\'';
        $this->db->exec($sql);
        $sql = 'UPDATE etymology2 SET sort_idx = substring(phrase, 2) WHERE LEFT(phrase, 1) = \'-\'';
        $this->db->exec($sql);

        // Display result
        if ($langs) {
            foreach ($langs as $key => $val) {
                if ($val['count']) {
                    echo(sprintf('%s: %s', $key, $val['count']) . LF);
                }
            }
        }
    }

    function get_kbbi_def()
    {
        $sql = 'SELECT abbrev, label FROM sys_abbrev WHERE type IN (\'lang\', \'region\');';
        if ($langs = $this->db->get_row_assoc($sql, 'abbrev', 'label')) {
            foreach ($langs as $key => $val) {
                $langs[$key] = array('label' => $val, 'count' => 0);
            }
        }
        $insert = 'INSERT INTO etymology2 (reference, in_kbbi, phrase, src_lang) VALUES (\'KBBI3\', 1, %s, %s)';
        $sql = 'SELECT a.phrase, a.discipline FROM definition a WHERE a.discipline IS NOT NULL;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                $infos = explode(',', $row['discipline']);
                foreach ($infos as $info) {
                    if (trim($info)) {
                        if (array_key_exists($info, $langs)) {
                            $sql = sprintf($insert,
                                $this->db->quote($row['phrase']),
                                $this->db->quote($info));
                            $this->db->exec($sql);
                            $langs[$info]['count']++;
                        }
                    }
                }
            }
        }
        $sql = 'UPDATE etymology2 SET sort_idx = phrase WHERE LEFT(phrase, 1) != \'-\'';
        $this->db->exec($sql);
        $sql = 'UPDATE etymology2 SET sort_idx = substring(phrase, 2) WHERE LEFT(phrase, 1) = \'-\'';
        $this->db->exec($sql);

        // Display result
        if ($langs) {
            foreach ($langs as $key => $val) {
                if ($val['count']) {
                    echo(sprintf('%s: %s', $key, $val['count']) . LF);
                }
            }
        }
    }

    function get_def_lwim()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.phrase_normal, a.raw_data FROM etymology a WHERE reference = \'LWIM\';';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                unset($this->sea->entries);
                $this->sea->raw_html = $row['raw_data'];
                $this->sea->parse_etymology();
                $sql = 'UPDATE etymology SET def_en = %s WHERE ety_id = %s;';
                foreach ($this->sea->entries as $entry) {
                    $sql = sprintf($sql,
                        $this->db->quote($entry['def']),
                        $row['ety_id']);
                    $this->db->exec($sql);
                    echo(sprintf('%s (%s/%s)', $entry['entry'],
                        $i + 1, $rec_num) . LF);
                }
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function get_subject()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.phrase_normal, a.raw_data ' .
            'FROM etymology a ' .
            'WHERE reference = \'LWIM\' AND raw_data LIKE \'%<usg type="subject"%\' ' .
            'ORDER BY a.phrase;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                unset($this->sea->entries);
                $this->sea->raw_html = $row['raw_data'];
                $this->sea->parse_etymology();
                foreach ($this->sea->entries as $entry) {
                    if ($entry['subject']) {
                        $insert = 'INSERT INTO tags (phrase, homonym, tag) VALUES (%s, %s, %s);';
                        $insert = sprintf($insert,
                            $this->db->quote($entry['entry']),
                            $this->db->quote($entry['hom']),
                            $this->db->quote($entry['subject']));
                        $this->db->exec($insert);
                        echo(sprintf('%s (%s/%s)', $entry['entry'],
                            $i + 1, $rec_num) . LF);
                    }
                }
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function get_variant()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.phrase_normal, a.raw_data ' .
            'FROM etymology a ' .
            'WHERE reference = \'LWIM\' AND raw_data LIKE \'%<xr type="var"%\' ' .
            'ORDER BY a.phrase;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                unset($this->sea->entries);
                $this->sea->raw_html = $row['raw_data'];
                $this->sea->parse_etymology();
                foreach ($this->sea->entries as $entry) {
                    if ($entry['variant']) {
                        $sql = 'UPDATE etymology SET variant = %s WHERE lwim = %s;';
                        $sql = sprintf($sql,
                            $this->db->quote($entry['variant']),
                            $this->db->quote($entry['lwim']));
                        //echo($sql . LF);
                        $this->db->exec($sql);
                        echo(sprintf('%s:%s (%s/%s)',
                            $entry['entry'],
                            $entry['variant'],
                            $i + 1, $rec_num) . LF);
                    }
                }
            }
        } else {
            echo('Oke!' . LF);
        }
    }

    function get_see()
    {
        $sql = 'SELECT a.ety_id, a.phrase, a.phrase_normal, a.raw_data ' .
            'FROM etymology a ' .
            'WHERE reference = \'LWIM\' AND raw_data LIKE \'%<xr type="see"%\' ' .
            'ORDER BY a.phrase;';
        if ($rows = $this->db->get_rows($sql)) {
            $rec_num = count($rows);
            for ($i = 0; $i < $rec_num; $i++) {
                $row = $rows[$i];
                unset($this->sea->entries);
                $this->sea->raw_html = $row['raw_data'];
                $this->sea->parse_etymology();
                foreach ($this->sea->entries as $entry) {
                    if ($entry['see']) {
//                        $sql = 'UPDATE etymology SET variant = %s WHERE lwim = %s;';
//                        $sql = sprintf($sql,
//                            $this->db->quote($entry['variant']),
//                            $this->db->quote($entry['lwim']));
//                        //echo($sql . LF);
//                        $this->db->exec($sql);
                        echo(sprintf('%s:%s (%s/%s)',
                            $entry['entry'],
                            $entry['see'],
                            $i + 1, $rec_num) . LF);
                    }
                }
            }
        } else {
            echo('Oke!' . LF);
        }
    }

} /* END CLASS */

