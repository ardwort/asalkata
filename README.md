# Apa

asalkata adalah upaya pendokumentasian asal kata dalam bahasa Indonesia.

# Siapa

* Ivan Lanin @ivanlanin "Kuncen"
* Jim Geovedi @geovedi "Heker"
* Rony Lantip @lantip "Perancang logo"
* Wahyu Ginting @wahyuginting "Munsyi"
* Adhi Ramdhan @mr_adhi "Penasihat linguistik"
* Ardwort @ardwort

# Di mana

asalkata adalah aplikasi web. Situs web tentu saja diletakkan di Internet.
Bila API sudah rampung, klien pengaksesnya bisa dibuat pada berbagai platform
lain seperti komputer, sabak, dan ponsel.

# Kapan

Berkas index.php pertama dibuat pada 3 Des 2013. Domain asalkata.com didaftarkan
pada 6 Des 2013 dan versi alfa situs diluncurkan pada 7 Des 2013 dengan data
dari LWIM dan KBBI3. Logo dibuatkan @lantip pada 8 Des.

# Mengapa

* Sejarah bahasa perlu didokumentasikan
* Sumber rujukan etimologi bahasa Indonesia masih sangat sedikit
* Kalau bukan kita, siapa lagi? Kalau bukan sekarang, kapan lagi?

# Bagaimana

Mengumpulkan bahan dari berbagai rujukan berikut:

* LWIM: [Loan-Words in Indonesian and Malay](http://sealang.net/lwim/)
* KBBI3: [Kamus Besar Bahasa Indonesia Edisi Ketiga (via Kateglo)](http://kateglo.com)

# Peer

* [David Moeljadi](http://getnocms.com/loanword_searchengine/browse_word)
* Penandaan homonim
* Pemberian tag "onomatope", "nama diri", dll
* Pencantuman makna kata agar bisa disebut "kamus" (Wahyu Ginting)
* Keterangan kapan mulai pertama digunakan (Wahyu Ginting)
* Tawaran entri lain saat tiada hasil yang sesuai (Wahyu Ginting)
* API!
* Kelas kata (duh, lama-lama jadi kamus beneran)

# Log

* 2013-12-11 10:51 Makna (live)
* 2013-12-11 09:55 Makna (beta)
* 2013-12-10 18:40 Tag
* 2013-12-09 18:22 Menandai kueri pencarian yang ditemukan (searched.found)
* 2013-12-09 17:32 Perbaikan homonim. Pembuatan tabel `words`
* 2013-12-09 00:53 Daftar bahasa pada bagian bawah
* 2013-12-08 17:54 Format baru dengan logo @lantip
* 2013-12-08 01:08 Log kueri pencarian
* 2013-12-07 21:25 Tautan kata ke http://kateglo.com
* 2013-12-07 21:25 Tautan asal kata bahasa Inggris ke http://etymonline.com
* 2013-12-07 21:20 Diakritik
* 2013-12-07 11:32 Usulan acak bila suatu kata tidak ditemukan
* 2013-12-07 10:13 Tautan pada rujukan yang sesuai
* 2013-12-07 10:13 Atribut "title" pada singkatan bahasa dan rujukan
* 2013-12-06 domain asalkata.com didaftarkan
* 2013-12-03 index.php dibuat
